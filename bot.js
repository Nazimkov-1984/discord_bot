const TOKEN = "";
const PREFIX = "!";
const { Client, Events, GatewayIntentBits } = require("discord.js");

const robot = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildMessageReactions,
    GatewayIntentBits.GuildMembers,
    GatewayIntentBits.MessageContent,
  ],
});

robot.on("ready", function () {
  console.log(robot.user.username + " запустився!");
});

robot.on(Events.MessageCreate, (message) => {
  if (!message.content.startsWith(PREFIX)) return;
  const command = message.content.replace(PREFIX, "");
  if (command === "react") {
    message.reply("Hello World");
  }
});

robot.login(TOKEN);
